;;; -*- no-byte-compile: t -*-
(define-package "swiper" "20190206.1951" "Isearch with an overview. Oh, man!" '((emacs "24.1") (ivy "0.11.0")) :commit "b01108e167ad26139535a516a056063d7ea4fff6" :keywords '("matching") :authors '(("Oleh Krehel" . "ohwoeowho@gmail.com")) :maintainer '("Oleh Krehel" . "ohwoeowho@gmail.com") :url "https://github.com/abo-abo/swiper")
